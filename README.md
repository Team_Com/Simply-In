Pour remplir la base avec des fausses données:
==

#CREER DES ROLES<br/>
insert into role (rolename) values('USER');<br/>
insert into role(rolename) values('ADMIN');<br/>

#CREER DES PROMOS<br/>
insert into promo (promoname) values('LaPoste1');<br/>
insert into promo (promoname) values('LaPoste2');<br/>

#CREER DES USERS<br/>
insert into user (username, password, email, github,firstname, lastname, phone, role_id, promo_id) values ('tom', 'aze', 'thomas@simplon.co', 'thomas@github.io', 'thomas', 'bu', '06.07.08.09.10', '1', '1');<br/>
insert into user (username, password, email, github,firstname, lastname, phone, role_id, promo_id) values ('bob', '123', 'bob@simplon.co', 'bob@github.io', 'bob', 'morane', '07.08.09.10.11', '1', '1');<br/>
insert into user (username, password, email, github,firstname, lastname, phone, role_id, promo_id) values ('admin', '123', 'admin@simplon.co', 'admin@github.io', 'admin', 'admin', '01.01.01.01.01', '2', '1');<br/>

#CREER DES TAGS<br/>
insert into tag (tagname) values ('CSS');<br/>
insert into tag (tagname) values ('JAVA');<br/>
insert into tag (tagname) values ('JS');<br/>

#CREER DES ARTICLES<br/>
insert into article (title, description, likecount, user_id) values ('Hello World', 'On todistettu, että lukijaa häiritsee sivun ulkoasu lukiessaan sivua.', '8','1');
insert into article (title, description, likecount, user_id) values ('Nouveautés', 'É um facto estabelecido de que um leitor é distraído pelo conteúdo legível de uma página quando analisa a sua mancha gráfica.', '4', '2');<br/>
insert into article (title, description, likecount, user_id) values ('Ouverture du site', 'On todistettu, että lukijaa häiritsee sivun ulkoasu lukiessaan sivua. Lorem Ipsumin käytön tarkoitus on, että se omaa enemmän-tai-vähemmän normaalimpaa sanojen sijoitusta kuin.', '7','3');<br/>

#CREER DES COMMENTS<br/>
insert into comment (content, article_id, user_id) values ('Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird', '1', '1');<br/>
insert into comment (content, article_id, user_id) values ('On todistettu, että lukijaa häiritsee sivun ulkoasu lukiessaan sivua.', '3', '2');<br/>
insert into comment (content, article_id, user_id) values ('Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird', '2', '1');<br/>
insert into comment (content, article_id, user_id) values ('Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird', '2', '1');<br/>

#CREER DES EVENTS<br/>
insert into event (title, description, url, user_id) values ('Ouverture du site', 'Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird', 'http://aaa.aa', '1');<br/>
insert into event (title, description, url, user_id) values ('JDK', 'On todistettu, että lukijaa häiritsee sivun ulkoasu lukiessaan sivua.', 'http://bbb.bb', '2');<br/>

#ASSOCIER TAGS A ARTICLES<br/>
insert into article_tag (tag_id, article_id) values ('1','1');<br/>
insert into article_tag (tag_id, article_id) values ('1','2');<br/>
insert into article_tag (tag_id, article_id) values ('2','3');<br/>
insert into article_tag (tag_id, article_id) values ('3','1');<br/>