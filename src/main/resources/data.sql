
insert into role (rolename) values('USER');
insert into role(rolename) values('ADMIN');


insert into promo (promoname) values('LaPoste1');
insert into promo (promoname) values('LaPoste2');


insert into user (username, password, email, github,firstname, lastname, phone, role_id, promo_id) values ('tom', 'aze', 'thomas@simplon.co', 'thomas@github.io', 'thomas', 'bu', '06.07.08.09.10', '1', '1');
insert into user (username, password, email, github,firstname, lastname, phone, role_id, promo_id) values ('bob', '123', 'bob@simplon.co', 'bob@github.io', 'bob', 'morane', '07.08.09.10.11', '1', '1');
insert into user (username, password, email, github,firstname, lastname, phone, role_id, promo_id) values ('admin', '123', 'admin@simplon.co', 'admin@github.io', 'admin', 'admin', '01.01.01.01.01', '2', '1');


insert into tag (tagname) values ('CSS');
insert into tag (tagname) values ('JAVA');
insert into tag (tagname) values ('JS');


insert into article (title, description, likecount, user_id) values ('Hello World', 'On todistettu, että lukijaa häiritsee sivun ulkoasu lukiessaan sivua.', '8','1');
insert into article (title, description, likecount, user_id) values ('Nouveautés', 'É um facto estabelecido de que um leitor é distraído pelo conteúdo legível de uma página quando analisa a sua mancha gráfica.', '4', '2');
insert into article (title, description, likecount, user_id) values ('Ouverture du site', 'On todistettu, että lukijaa häiritsee sivun ulkoasu lukiessaan sivua. Lorem Ipsumin käytön tarkoitus on, että se omaa enemmän-tai-vähemmän normaalimpaa sanojen sijoitusta kuin.', '7','3');


insert into comment (content, article_id, user_id) values ('Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird', '1', '1');
insert into comment (content, article_id, user_id) values ('On todistettu, että lukijaa häiritsee sivun ulkoasu lukiessaan sivua.', '3', '2');
insert into comment (content, article_id, user_id) values ('Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird', '2', '1');
insert into comment (content, article_id, user_id) values ('Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird', '2', '1');


insert into event (title, description, url, user_id) values ('Ouverture du site', 'Es ist ein lang erwiesener Fakt, dass ein Leser vom Text abgelenkt wird', 'http://aaa.aa', '1');
insert into event (title, description, url, user_id) values ('JDK', 'On todistettu, että lukijaa häiritsee sivun ulkoasu lukiessaan sivua.', 'http://bbb.bb', '2');


insert into article_tag (tag_id, article_id) values ('1','1');
insert into article_tag (tag_id, article_id) values ('1','2');
insert into article_tag (tag_id, article_id) values ('2','3');
insert into article_tag (tag_id, article_id) values ('3','1');