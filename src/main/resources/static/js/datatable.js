$(document).ready( function () {
	 var table = $('#usersTable').DataTable({
			"sAjaxSource": "/users",
			"sAjaxDataProp": "",
			"columnDefs": [ { "defaultContent": "-", "targets": "_all" } ],
			"searching": false,
			"aoColumns": [
			    { "mData": "id"},
			    { "mData": "username" },
			    { "mData": "firstname" },
			    { "mData": "lastname" },
			    { "mData": "email" },
			    { "mData": "github" },
			    { "mData": "phone" },
			    { "mData": "role.rolename" },
			    { "mData": "promo.promoname" }
			]
	 })
});