package co.simplyIn.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Promo {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String promoname;
	
	@OneToMany(mappedBy = "promo")
    private List<User> user;
	
	
	//**********Constructor; Getters,Setters*************************
	public Promo() {}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPromoname() {
		return promoname;
	}
	public void setPromoname(String promoname) {
		this.promoname = promoname;
	}
	
	public List<User> getUser() {
		return user;
	}
	public void setUser(List<User> user) {
		this.user = user;
	}		
	
}
