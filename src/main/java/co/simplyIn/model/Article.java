package co.simplyIn.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Article {

	@Id
	@GeneratedValue
	private Long id;
	
	private String title;
	private String description;
	private int likecount;
	
	@CreationTimestamp
	private Timestamp datecreation;
	
	@UpdateTimestamp
	private Timestamp datemaj;

	
	@ManyToOne    
	private User user;	
	
	@ManyToMany
	@JoinTable(name="article_tag", joinColumns= {
			@JoinColumn(name="article_id")},
	inverseJoinColumns = {@JoinColumn(name="tag_id")})
	private List<Tag> tag;
	
    @OneToMany(mappedBy = "article")
	private List<Comment> comment;
	
    
	//**********Constructor; Getters,Setters*************************
	public Article() {}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getLike() {
		return likecount;
	}
	public void setLike(int likecount) {
		this.likecount = likecount;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public List<Tag> getTag() {
		return tag;
	}
	public void setTag(List<Tag> tag) {
		this.tag = tag;
	}

	public List<Comment> getComment() {
		return comment;
	}
	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}
	
	public Timestamp getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Timestamp datecreation) {
		this.datecreation = datecreation;
	}

	public Timestamp getDatemaj() {
		return datemaj;
	}
	public void setDatemaj(Timestamp datemaj) {
		this.datemaj = datemaj;
	}
}
