package co.simplyIn.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Comment {

	@Id
	@GeneratedValue 
	private Long id;
	
	private String content;
	
	@CreationTimestamp
	private Timestamp datecreation;
	
	@UpdateTimestamp
	private Timestamp datemaj;

	@ManyToOne
//    @JoinColumn(name="user_id")
	private User user;
	
	
	@ManyToOne
//    @JoinColumn(name="article_id")
	private Article article;
	
	
	//**********Constructor; Getters,Setters*************************
	public Comment() {}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	
	public Timestamp getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Timestamp datecreation) {
		this.datecreation = datecreation;
	}

	public Timestamp getDatemaj() {
		return datemaj;
	}
	public void setDatemaj(Timestamp datemaj) {
		this.datemaj = datemaj;
	}
}
