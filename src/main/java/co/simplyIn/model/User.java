package co.simplyIn.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
   	
	@Id
	@GeneratedValue  
    private Long id;
	
    private String username;
    @JsonIgnore
    private String password; 
    private String email; 
    private String github;  
    private String firstname;
    private String lastname;  
    private String phone;
    
    @ManyToOne
    private Role role;
    
    @ManyToOne
    private Promo promo;
      
    @OneToMany(mappedBy = "user")
    private List<Article> article;
    
    @OneToMany(mappedBy = "user")
    private List<Comment> comment;
    
    @OneToMany(mappedBy = "user")
    private List<Event> event;

	
	//**********Constructor; Getters,Setters*************************
	public User() { }
    

	public Long getId() {
        return id;
    }
	public void setId(Long id) {
		this.id = id;
	}
	
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getGithub() {
        return github;
    }
    public void setGithub(String github) {
        this.github = github;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

    public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
    
	public Promo getPromo() {
		return promo;
	}
	public void setPromo(Promo promo) {
		this.promo = promo;
	}

	public List<Article> getArticle() {
		return article;
	}
	public void setArticle(List<Article> article) {
		this.article = article;
	}
	
	public List<Comment> getComment() {
		return comment;
	}
	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}
	
	public List<Event> getEvent() {
		return event;
	}
	public void setEvent(List<Event> event) {
		this.event = event;
	}
	
}