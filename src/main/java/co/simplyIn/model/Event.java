package co.simplyIn.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Event {

	@Id
	@GeneratedValue
	private Long id;
	
	private String title;
	private String description;
	private String url;
	
	@CreationTimestamp
	private Timestamp datecreation;
	
	@UpdateTimestamp
	private Timestamp datemaj;
	
	@ManyToOne
//    @JoinColumn(name="user_id")
	private User user;
	
	
	//**********Constructor; Getters,Setters*************************
	public Event() {}

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Timestamp getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Timestamp datecreation) {
		this.datecreation = datecreation;
	}
	
	public Timestamp getDatemaj() {
		return datemaj;
	}
	public void setDatemaj(Timestamp datemaj) {
		this.datemaj = datemaj;
	}
}
