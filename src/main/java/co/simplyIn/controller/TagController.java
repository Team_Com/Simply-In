package co.simplyIn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplyIn.model.Tag;
import co.simplyIn.service.TagService;

@RestController
@RequestMapping("/tags")
public class TagController {
	
	@Autowired
	TagService tagService;
	
	@GetMapping
	public Iterable<Tag> findAll() {
		return tagService.getAll();
	}
	
	@PostMapping
	public Tag saveOne(@RequestBody Tag tag) {
		return tagService.saveOne(tag);
	}

}
