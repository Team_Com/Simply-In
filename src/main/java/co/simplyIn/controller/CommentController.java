package co.simplyIn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplyIn.model.Comment;
import co.simplyIn.service.CommentService;

@RestController
@RequestMapping("/comments")
public class CommentController {

	@Autowired
	  private CommentService commentService;
	
	@GetMapping
	public Iterable<Comment> findAll() {
		return commentService.getAll();
	}
	
	@PostMapping
	public Comment saveOne(@RequestBody Comment comment) {
		return commentService.saveOne(comment);
	}
	
	@PutMapping
	public Comment updateOne(@RequestBody Comment comment) {
		return updateOne(comment);
	}
}
