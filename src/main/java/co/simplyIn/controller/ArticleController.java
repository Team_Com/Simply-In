package co.simplyIn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplyIn.model.Article;
import co.simplyIn.service.ArticleService;


@RestController
@RequestMapping("/articles")
public class ArticleController {

	@Autowired
	  private ArticleService articleService;
	
	@GetMapping
    public Iterable<Article> findAll() {
		return articleService.getAll();
    }
	
	@PostMapping
	public Article saveOne(@RequestBody Article article) {
		return articleService.saveOne(article);
	}
	
	@PutMapping
	public Article updateOne(@RequestBody Article article) {
		return articleService.updateOne(article);
	}
}
