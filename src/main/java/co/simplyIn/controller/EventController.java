package co.simplyIn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplyIn.model.Event;
import co.simplyIn.service.EventService;

@RestController
@RequestMapping("/events")
public class EventController {
	
	@Autowired
	EventService eventService;
	
	@GetMapping
	public Iterable<Event> findAll() {
		return eventService.getAll();
	}

	@PostMapping
	public Event saveOne(@RequestBody Event event) {
		return eventService.saveOne(event);
	}
	
}
