package co.simplyIn.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplyIn.model.Article;
import co.simplyIn.model.Comment;
import co.simplyIn.model.Promo;
import co.simplyIn.model.Tag;
import co.simplyIn.model.User;
import co.simplyIn.repository.ArticleRepository;

@Service
public class ArticleService {

	@Autowired
	private ArticleRepository articleRepository;
	
	public List<Article>  getAll() {
		Iterable<Article> request = articleRepository.findAll();			
		List<Article> articleList = new ArrayList<>();
		for (Article a : request) {
			Article article = new Article();
			article.setId(a.getId());
			article.setTitle(a.getTitle());
			article.setDescription(a.getDescription());
			article.setLike(a.getLike());
			article.setDatecreation(a.getDatecreation());
			article.setDatemaj(a.getDatemaj());
			
			article.setComment(displayComment(a.getComment()));
			article.setTag(displayTag(a.getTag()));
				
			User user = new User();
			user.setId(a.getUser().getId());
			user.setUsername(a.getUser().getUsername());
			Promo promo = new Promo();
			promo.setId(a.getUser().getPromo().getId());
			promo.setPromoname(a.getUser().getPromo().getPromoname());
			user.setPromo(promo);
			article.setUser(user);
			
			
			
			articleList.add(article);
		}
		return articleList;			
	}
	
	public Article saveOne(Article article) {
		return articleRepository.save(article);
	}
	
	//UPDATE LIKECOUNT
	public Article updateOne(Article article) {
		Article request = articleRepository.findById(article.getId());
		request.setLike(article.getLike());
		articleRepository.save(request);
		
		Article updatedArticle = new Article();
		updatedArticle.setLike(request.getLike());
		
		return updatedArticle;
	}
	
	//AFFICHER LES TAGS POUR LES ARTICLES
	private List<Tag> displayTag(List<Tag> tagList) {
		List<Tag> tagResult = new ArrayList<>();
		for (Tag t : tagList) {
		Tag tag = new Tag();
		tag.setId(t.getId());
		tag.setTagname(t.getTagname());
		tagResult.add(tag);
		}
	return tagResult;
	}
	
	//AFFICHER LES COMMENTAIRES POUR LES ARTICLES
	private List<Comment> displayComment(List<Comment> commentList) {
		List<Comment> commentResult = new ArrayList<>();
		for (Comment c : commentList) {
			Comment comment = new Comment();
			comment.setId(c.getId());
			comment.setContent(c.getContent());
			comment.setDatecreation(c.getDatecreation());
			
			if(c.getUser() != null) {
			User user = new User();
			user.setId(c.getUser().getId());
			user.setUsername(c.getUser().getUsername());
			comment.setUser(user);
			}
			commentResult.add(comment);
		}
		return commentResult;
	}
	
}
