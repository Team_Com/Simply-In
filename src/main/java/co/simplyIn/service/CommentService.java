package co.simplyIn.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplyIn.model.Article;
import co.simplyIn.model.Comment;
import co.simplyIn.model.Promo;
import co.simplyIn.model.User;
import co.simplyIn.repository.CommentRepository;

@Service
public class CommentService {

	@Autowired
	CommentRepository commentRepository;
	
	public List<Comment> getAll() {
		Iterable<Comment> request = commentRepository.findAll();
		List<Comment> commentList = new ArrayList<>();
		for(Comment c : request) {
			Comment comment = new Comment();
			comment.setId(c.getId());
			comment.setContent(c.getContent());
			comment.setDatecreation(c.getDatecreation());
			comment.setDatemaj(c.getDatemaj());
			
				User user = new User();
				user.setId(c.getUser().getId());
				user.setUsername(c.getUser().getUsername());
				Promo promo = new Promo();
				promo.setId(c.getUser().getPromo().getId());
				promo.setPromoname(c.getUser().getPromo().getPromoname());
				user.setPromo(promo);
				comment.setUser(user);
				
				Article article = new Article();
				article.setId(c.getArticle().getId());
				article.setLike(c.getArticle().getLike());
				comment.setArticle(article);
				
			commentList.add(comment);
		}
		return commentList;
	}
	
	public Comment saveOne(Comment comment) {
		return commentRepository.save(comment);
	}
}
