package co.simplyIn.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplyIn.model.Promo;
import co.simplyIn.model.Role;
import co.simplyIn.model.User;
import co.simplyIn.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	//FIND ALL USER****************************
	public List<User> fillListOfUser() { 
		Iterable<User> request = userRepository.findAll();
		List<User> allUser = new ArrayList<>();
		for (User u : request) {
			User user = new User();
			user.setId(u.getId());
			user.setUsername(u.getUsername());
			user.setFirstname(u.getFirstname());
			user.setLastname(u.getLastname());
			user.setEmail(u.getEmail());
			user.setGithub(u.getGithub());
			user.setPhone(u.getPhone());
			
			// sinon peut renvoyer un null execption
			if(u.getPromo() != null) {
			Promo promo = new Promo();
			promo.setId(u.getPromo().getId());
			promo.setPromoname(u.getPromo().getPromoname());
			user.setPromo(promo);
			}	
			if(u.getRole() != null) {
			Role role = new Role();
			role.setId(u.getRole().getId());
			role.setRolename(u.getRole().getRolename());
			user.setRole(role);
			}
			
			allUser.add(user);
		}
		return allUser;
	}
	
	//SAVE USER********************************
	public User saveOne(User user) {
		return userRepository.save(user);
	}
	
	//UPDATE USER******************************
	public User updateOne(User user) {
		User request = userRepository.findById(user.getId());
		//tester si il existe une valeur pour ne pas l ecraser par un null
		if(user.getUsername() != null) {
			request.setUsername(user.getUsername());
		}
    	if(user.getFirstname() != null) {
    		request.setFirstname(user.getFirstname());
    	}
    	if(user.getLastname() != null) {
    		request.setLastname(user.getLastname());
    	}
    	if(user.getPhone() != null) {
    		request.setPhone(user.getPhone());
    	}
    	if(user.getEmail() != null) {
    		request.setEmail(user.getEmail());
    	}
    	if(user.getGithub() != null) {
    		request.setGithub(user.getGithub());
    	}
    	if(user.getRole() != null) {
    		request.setRole(user.getRole());	
    	}
    	if(user.getPromo() != null) {
    		request.setPromo(user.getPromo());
    	}
    	userRepository.save(request);
    	
		User updatedUser = new User();
		
		if(request.getRole() != null) {
		Role role = new Role();		
		role.setId(request.getRole().getId());
		role.setRolename(request.getRole().getRolename());
		updatedUser.setRole(role);
		}
		if(request.getPromo() != null) {
		Promo promo = new Promo();
		promo.setId(request.getPromo().getId());
		promo.setPromoname(request.getPromo().getPromoname());
		updatedUser.setPromo(promo);
		}
		
		updatedUser.setId(request.getId());
		updatedUser.setFirstname(request.getFirstname());
		updatedUser.setLastname(request.getLastname());
		updatedUser.setEmail(request.getEmail());
		updatedUser.setGithub(request.getGithub());
		updatedUser.setPhone(request.getPhone());			
		
			return updatedUser;
		
	}
	
}
