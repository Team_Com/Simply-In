package co.simplyIn.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplyIn.model.Article;
import co.simplyIn.model.Tag;
import co.simplyIn.repository.TagRepository;

@Service
public class TagService {

	@Autowired
	TagRepository tagRepository;
	
	public List<Tag> getAll() {
		Iterable<Tag> request = tagRepository.findAll();
		List<Tag> tagList = new ArrayList<>();
		for(Tag t : request) {
			Tag tag = new Tag();
			tag.setId(t.getId());
			tag.setTagname(t.getTagname());
			tag.setArticle(displayArticle(t.getArticle()));
			
			tagList.add(tag);
		}
		return tagList;
	}
	
	public Tag saveOne(Tag tag) {
		return tagRepository.save(tag);
	}
	
	//AFFICHER LES ARTICLES POUR LES TAGS
	private List<Article> displayArticle(List<Article> articleList) {
		List<Article> articleResult = new ArrayList<>();
		for(Article a : articleList) {
			Article article = new Article();
			article.setId(a.getId());
			article.setTitle(a.getTitle());
			article.setDescription(a.getDescription());
			article.setLike(a.getLike());
			articleResult.add(article);
		}
		return articleResult;	
	}
}
