package co.simplyIn.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplyIn.model.Event;
import co.simplyIn.model.Promo;
import co.simplyIn.model.User;
import co.simplyIn.repository.EventRepository;

@Service
public class EventService {

	@Autowired
	private EventRepository eventRepository;
	
	public List<Event> getAll() {
		Iterable<Event> request = eventRepository.findAll();
		List<Event> eventList = new ArrayList<>();
		for(Event e : request) {
			Event event = new Event();
			event.setId(e.getId());
			event.setTitle(e.getTitle());
			event.setDescription(e.getDescription());
			event.setUrl(e.getUrl());
			event.setDatecreation(e.getDatecreation());
			event.setDatemaj(e.getDatemaj());
			
			User user = new User();
			user.setId(e.getUser().getId());
			user.setUsername(e.getUser().getUsername());
			Promo promo = new Promo();
			promo.setId(e.getUser().getPromo().getId());
			promo.setPromoname(e.getUser().getPromo().getPromoname());
			user.setPromo(promo);
			event.setUser(user);
			
			
			eventList.add(event);
		}
		return eventList;
	}
	
	public Event saveOne(Event event) {
		return eventRepository.save(event);
	}
}
