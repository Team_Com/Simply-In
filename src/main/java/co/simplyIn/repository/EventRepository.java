package co.simplyIn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.simplyIn.model.Event;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

	public Event findById(Long id);
}
