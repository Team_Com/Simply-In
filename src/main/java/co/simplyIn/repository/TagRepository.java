package co.simplyIn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.simplyIn.model.Tag;

@Repository
public interface TagRepository extends CrudRepository<Tag, Long>{

	public Tag findById(Long id);
}
