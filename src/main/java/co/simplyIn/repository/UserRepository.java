package co.simplyIn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.simplyIn.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    
	public User findById(Long id);
    
}