package co.simplyIn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.simplyIn.model.Promo;

@Repository
public interface PromoRepository extends CrudRepository<Promo, Long> {

}
