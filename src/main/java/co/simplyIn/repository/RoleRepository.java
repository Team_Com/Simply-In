package co.simplyIn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.simplyIn.model.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

}
