package co.simplyIn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.simplyIn.model.Comment;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {

	public Comment findByUserId(Long userId);

}
