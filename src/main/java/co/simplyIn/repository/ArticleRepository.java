package co.simplyIn.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.simplyIn.model.Article;


@Repository
public interface ArticleRepository extends CrudRepository<Article, Long>{

	public Article findById(Long id);
}
